/*
	* File:	main.cpp
	* Author: TheGrieve
	*
	* Created on 28 May 2010, 20:08
	*/

/*
	*
	*/

#include <iostream>
#include "../stile/Stile.h"

#include <time.h>

stile::Control*	gControl;
stile::Engine* gEngine;

void init
(
	int argc,
	char** argv
)
{
}

void startBasicTest()
{
	gControl->log->Warning("Starting engine test...");
	stile::World* world = new stile::World("Test World");
	world->Debug();

	gEngine = new stile::Engine("",1280,720,10,10);
	gEngine->SetWorld(world);

	sf::Image* image = new sf::Image();
	image->LoadFromFile("default.png");
	stile::Entity* testBox;

	testBox = new stile::Entity(630, 350, 20, 20, image);
	testBox->SetOrigin(10, 10);
	testBox->SetAngularVelocity(0.2);
	world->Add(testBox);

	testBox = new stile::Entity(20, 200, 20, 20, image);
	testBox->SetOrigin(10, 10);
	testBox->SetAcceleration(0.001, 0);
	testBox->SetAngularVelocity(-0.5);
	world->Add(testBox);
}

void timerTrigger(
		unsigned int indentifier,
		const char* strIdentifier,
		unsigned int millis
	)
{
	if(strcmp(strIdentifier, "EngineTest") == 0)
	{
		startBasicTest();
	}

	if(strcmp(strIdentifier, "EngineFast") == 0)
	{
		gControl->log->Warning("Increasing engine rates...");
		gEngine->SetFrameRate(60);
		gEngine->SetUpdateRate(100);
	}

	if(strcmp(strIdentifier, "EngineEnd") == 0)
	{
		gEngine->Stop();
		gControl->log->Warning("All tests over. Exiting in 5 seconds.");
		gControl->time->AddTrigger(timerTrigger, "Exit", 5000);
	}

	if(strcmp(strIdentifier, "Exit") == 0)
	{
		gControl->event->SetGameState(stile::GAME_STATE_OVER);
	}
}

void handleGameStateChange(
		stile::GameState gameState
	)
{
	if(gameState == stile::GAME_STATE_OVER)
	{
		gControl->log->Info("Game state has changed to GAME_STATE_OVER. Quitting.");
		gControl->event->Shutdown(0);
	}
}

int main(
		int argc,
		char** argv
	)
{
	gControl = &stile::GetControl();
	gControl->log->EnableStdOut(true);
	init(argc, argv);
	gControl->event->AddGameStateCallback(handleGameStateChange);
	gControl->log->Info("\n\
In 10 seconds we will begin the prototype Engine, World \
and Entity tests. A world will be created and added to the engine. 2 entities \
will then be created and added to the world. This simiulation will then run for 5 \
seconds at a very slow render and update rate. We will then increase these \
rates to something more realistic for 15 seconds.");
	gControl->time->AddTrigger(timerTrigger, "EngineTest", 10000);
	gControl->time->AddTrigger(timerTrigger, "EngineFast", 15000);
	gControl->time->AddTrigger(timerTrigger, "EngineEnd", 30000);
	while(true)
	{
		gControl->time->Update();
	}
}
