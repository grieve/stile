#include "Vectors.h"

namespace stile
{
	namespace math
	{
	  Vector2D::Vector2D (float _x, float _y)
	  {
	    x = _x;
	    y = _y;
	  }

	  Vector2D::~Vector2D ()
	  {
	  }

	  Vector2D Vector2D::operator* (float scalar) const
	  {
	    return Vector2D(x * scalar, y * scalar);
	  }

	  Vector2D Vector2D::operator+ (const Vector2D &vect) const
	  {
	    return Vector2D(x + vect.x, y + vect.y);
	  }

	  Vector2D Vector2D::operator- (const Vector2D &vect) const
	  {
	    return Vector2D(x - vect.x, y - vect.y);
	  }

	  void Vector2D::operator*= (float scalar)
	  {
	    x *= scalar;
	    y *= scalar;
	  }

	  void Vector2D::operator+= (const Vector2D &vect)
	  {
	    x += vect.x;
	    y += vect.y;
	  }

	  void Vector2D::operator-= (const Vector2D &vect)
	  {
	    x -= vect.x;
	    y -= vect.y;
	  }

	  void Vector2D::Rotate (float angle)
	  {
	    float xt = (x * cosf(angle)) - (y * sinf(angle));
	    float yt = (y * cosf(angle)) + (x * sinf(angle));
	    x = xt;
	    y = yt;
	  }

	  float Vector2D::Cross (const Vector2D &vect2) const
	  {
	    return (this->x * vect2.y) - (this->y * vect2.x);
	  }

	  float Vector2D::Mag()
	  {
	    return sqrtf(x * x +y * y);
	  }

	  void Vector2D::Norm()
	  {
	    float mag = sqrtf(x* x + y * y);
	    this->x = x / mag;
	    this->y = y / mag;
	  }

	  float Vector2D::Dot (const Vector2D &vect) const
	  {
	    return (x * vect.x) + (y * vect.y);
	  }

	  float Vector2D::GetAngle()
	  {
	    if ( y == 0)
	      if (x > 0)
	        return (0.5*M_PI);
	      if (x < 0)
	        return (1.5*M_PI);
	      else
	        return 0;

	    float ang = atan(x/y);
	    if (x > 0)
	      if (y > 0)
	        return ang;
	      else
	        return M_PI-ang;
	    else
	      if (y > 0)
	        return (2*M_PI)-ang;
	      else
	        return M_PI+ang;
	  }
	}
}
