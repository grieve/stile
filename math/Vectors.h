#ifndef STILE_VECTOR
#define STILE_VECTOR

#include <iostream>
#include <string>
#include <math.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

namespace stile
{
	namespace math
	{
	  class Vector2D;
	  class Vector3D;
	}
}

class stile::math::Vector2D
{
public:

	          Vector2D    (float x = 0, float y = 0);
	          ~Vector2D   ();
	Vector2D  operator*   (float scalar) const;
	Vector2D  operator+   (const Vector2D &vect) const;
	Vector2D  operator-   (const Vector2D &vect) const;
	void      operator*=  (float scalar);
	void      operator+=  (const Vector2D &vect);
	void      operator-=  (const Vector2D &vect);
	void      Rotate      (float angle);
	float     Cross       (const Vector2D &vect2) const;
	float     Mag         ();
	void      Norm        ();
	float     Dot         (const Vector2D &vect) const;
	float     GetAngle    ();

	float     x;
	float     y;
};

class stile::math::Vector3D
{
public:

	          Vector3D      (float x = 0, float y = 0, float z = 0);
	          ~Vector3D     ();

	float     x;
	float     y;
	float     z;
};

#endif //STILE_VECTOR
