STILE_SRCS =	\
  ../stile/Stile.cc \
	../stile/core/Configurator.cc \
	../stile/core/EventManager.cc \
	../stile/core/Logger.cc \
	../stile/core/Timer.cc \
	../stile/core/Entity.cc \
	../stile/core/World.cc \
	../stile/core/Engine.cc \
	../stile/math/Vectors.cc

STILE_OBJS = $(STILE_SRCS:.cc=.o)

DEPS += stile
DEPS_LINKS += stile.a
LIBS += -lsfml-audio -lsfml-graphics -lsfml-window -lsfml-system

stile:
	$(CC) $(CFLAGS) $(STILE_SRCS) $(LIBS)
	ar rc stile.a $(notdir $(STILE_OBJS))
	ranlib stile.a
