CC = colorgcc
DEBUG = -g
CFLAGS = -Wall $(DEBUG) -c -I ../
LFLAGS = -Wall $(DEBUG) 

PROJECT = stile_test
DEPS = 
DEP_LINKS = 
LIBS = 

PROJECT_SRCS  = main.cc
PROJECT_OBJS = $(PROJECT_SRCS:.cc=.o)

default: $(PROJECT)

include Makefile.inc

$(PROJECT) : $(DEPS)
	$(CC) $(CFLAGS) $(PROJECT_SRCS)
	$(CC) $(LFLAGS) $(PROJECT_OBJS) $(DEPS_LINKS) $(LIBS) -o $(PROJECT)

clean :
	\rm -f *.so *.o *.a *~ */*.o */*~ */*.a *.bin $(PROJECT)
