#include "Stile.h"

stile::Control::Control()
{
	time = new stile::Timer();
	log	= new stile::Logger(time);
	config = new stile::Configurator(time, log, "stile.ini");
	event = new stile::EventManager(time, log, config);
}

stile::Control& stile::GetControl()
{
	static Control control;
	return control;
}
