#ifndef STILE_ENTITY
#define STILE_ENTITY

#include "stile/Stile.h"
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Image.hpp>
#include <SFML/Graphics/Shape.hpp>
#include "stile/math/Vectors.h"

namespace stile
{
	class	Entity;
	class	Engine;
	struct	Control;
}

class stile::Entity : public sf::Sprite
{
private:
	stile::Control&			_control;
	sf::Shape				_bounding;
	bool					_enabled;
	char					_name[128];
	stile::math::Vector2D	_velocity;
	stile::math::Vector2D	_acceleration;
	stile::math::Vector2D	_drag;
	float					_angularVelocity;


public:

									Entity				(
															float x = 0,
															float y = 0,
															float w = 20,
															float h = 20,
															sf::Image* image = NULL
														);
	virtual							~Entity				();

	virtual void					Update				(unsigned int elapsed);
	virtual void					Render				(sf::RenderTarget* target);
	virtual void					DebugDraw			(sf::RenderTarget* target);

			void					UpdateBoundingBox	();
			void					SetAngularVelocity	(float velocity);
			float					GetAngularVelocity	();

			void					SetSize				(float x, float y);
			void					SetSize				(stile::math::Vector2D size);
			void					SetVelocity			(float x, float y);
			void					SetVelocity			(stile::math::Vector2D velocity);
			void					SetAcceleration		(float x, float y);
			void					SetAcceleration		(stile::math::Vector2D acceleration);
			void					SetDrag				(float x, float y);
			void					SetDrag				(stile::math::Vector2D drag);
			void					SetOrigin			(float x, float y);
			void					SetOrigin			(stile::math::Vector2D center);

			void					SetImage			(sf::Image* image);
			sf::Image*				GetImage			();


			stile::math::Vector2D	GetSize				();
			stile::math::Vector2D	GetVelocity			();
			stile::math::Vector2D	GetAcceleration		();
			stile::math::Vector2D	GetDrag				();
			stile::math::Vector2D	GetOrigin			();


			stile::math::Vector2D	Accelerate			(float x, float y);
			stile::math::Vector2D	Accelerate			(stile::math::Vector2D acceleration);

			stile::math::Vector2D	MoveBy				(float x, float y);
			stile::math::Vector2D	MoveBy				(stile::math::Vector2D move);
			stile::math::Vector2D	MoveTo				(float x, float y);
			stile::math::Vector2D	MoveTo				(stile::math::Vector2D move);

			void					Rotate				(float rot);
};

#endif //STILE_ENTITY
