
#include "Engine.h"

stile::Engine::Engine
	(
	  const char*   title,
	  unsigned int  width,
	  unsigned int  height,
	  unsigned int  updateRate,
	  unsigned int  frameRate
	)
	:_control(stile::GetControl())
{
	_window = new sf::RenderWindow(sf::VideoMode(width, height, 32), title);
	_updateRate = updateRate;
	_control.time->AddTrigger(this, 1, 1000/_updateRate, true);
	_frameRate = frameRate;
	_control.time->AddTrigger(this, 2, 1000/_frameRate, true);
	_paused = false;
	_stopped = false;
};

stile::Engine::~Engine  ()
{
}

void stile::Engine::Render  ()
{
	if(_world && !_stopped)
	{
	  _window->Clear();
	  _world->Render(_window);
	  _window->Display();
	}
}

void stile::Engine::Update  (unsigned int elapsed)
{
	_control.event->ProcessWindowEvents(_window);
	if(_world && !_paused)
	  _world->Update(elapsed);
}

void stile::Engine::SetWorld  (
	  stile::World* world
	)
{
	_world = world;
}

stile::World* stile::Engine::GetWorld  ()
{
	return _world;
}

void stile::Engine::Stop  ()
{
	_stopped = true;
	_paused = true;
}

void stile::Engine::Pause  ()
{
	_paused = true;
}

void stile::Engine::Resume  ()
{
	_stopped = false;
	_paused = false;
}

void stile::Engine::SetFrameRate  (
	  unsigned int frameRate
	)
{
	_frameRate = frameRate;
	_control.time->RemoveTrigger(2);
	_control.time->AddTrigger(this, 2, 1000/_frameRate, true);
}

void stile::Engine::SetUpdateRate  (
	  unsigned int updateRate
	)
{
	_updateRate = updateRate;
	_control.time->RemoveTrigger(1);
	_control.time->AddTrigger(this, 1, 1000/_updateRate, true);
}

void stile::Engine::Trigger  (
	  unsigned int  id,
	  const char*  strId,
	  unsigned int  millis
	)
{
	switch(id)
	{
	  case 1:
	    Update(millis);
	    break;
	  case 2:
	    Render();
	    break;
	}
}
