/*
&&LICENSE
COPYRIGHT (C)2010 - RYAN GRIEVE
LICENSE&&
*/

#include "Timer.h"
#include <iostream>


struct stile::Timer::TimerImpl
{
public:
	struct TriggerObject
	{
	public:
	  unsigned int          milliseconds;
	  unsigned int          clocks;
	  unsigned int          identifier;
	  char                  strIdentifier[128];
	  bool                  repeat;
	  bool                  allowDrift;
	  TriggerCallback       triggerCallback;
	  stile::TimerHandler*  handler;
	};

	typedef std::multimap<unsigned int, TriggerObject*> TriggerMap;

	Timer&        _parent;
	unsigned long _clocks;
	float         _seconds;
	float         _millis;
	TriggerMap    _triggerMap;


	TimerImpl  (
	    Timer* parent
	  )
	  : _parent (*parent)
	{};

	~TimerImpl  ()
	{};

	void  AddTrigger  (
	                    TriggerCallback callback,
	                    const char* identifier,
	                    unsigned int milliseconds,
	                    bool repeat,
	                    bool allowDrift
	                  );
	void  AddTrigger  (
	                    TriggerCallback callback,
	                    unsigned int identifier,
	                    unsigned int milliseconds,
	                    bool repeat,
	                    bool allowDrift
	                  );
	void  AddTrigger  (
	                    stile::TimerHandler* handler,
	                    const char* identifier,
	                    unsigned int milliseconds,
	                    bool repeat,
	                    bool allowDrift
	                  );
	void  AddTrigger  (
	                    stile::TimerHandler* handler,
	                    unsigned int identifier,
	                    unsigned int milliseconds,
	                    bool repeat,
	                    bool allowDrift
	                  );

	void  RemoveTrigger   (const char* identifier);
	void  RemoveTrigger   (unsigned int identifier);
	void  RemoveTrigger   (stile::TimerHandler* handler);
	void  Update          ();
	void  UpdateTriggers  ();
};

void stile::Timer::TimerImpl::Update()
{
	_seconds = (float)clock() / CLOCKS_PER_SEC;
	UpdateTriggers();
}

void stile::Timer::TimerImpl::UpdateTriggers ()
{
	if (_triggerMap.size() == 0)
	{
	  return;
	}
	while (_triggerMap.begin()->first < clock())
	{
	  TriggerObject* trig = _triggerMap.begin()->second;
	  if (trig->milliseconds > 0)
	  {
	    if (trig->handler)
	    {
	      trig->handler->Trigger(trig->identifier, trig->strIdentifier, trig->milliseconds);
	    }
	    else
	    {
	      trig->triggerCallback(trig->identifier, trig->strIdentifier, trig->milliseconds);
	    }
	    if (trig->repeat)
	    {
	      unsigned int newClocks;
	      if (!trig->allowDrift)
	      {
	        newClocks = (clock()+trig->clocks)-(clock()-_triggerMap.begin()->first);
	      }
	      else
	      {
	        newClocks = clock()+trig->clocks;
	      }
	      _triggerMap.insert(TriggerMap::value_type(newClocks,trig));
	    }
	  }
	  _triggerMap.erase(_triggerMap.begin());
	}
}

void stile::Timer::TimerImpl::AddTrigger (
	  TriggerCallback callback,
	  const char* identifier,
	  unsigned int milliseconds,
	  bool repeat,
	  bool allowDrift
	)
{
	assert(identifier[0] != '\0');
	TriggerObject* trig = new TriggerObject;
	strcpy(trig->strIdentifier,identifier);
	trig->milliseconds = milliseconds;
	trig->clocks = (unsigned int)floor((CLOCKS_PER_SEC/1000)*milliseconds);
	trig->repeat = repeat;
	trig->allowDrift = allowDrift;
	trig->triggerCallback = callback;
	trig->handler = NULL;
	_triggerMap.insert (TriggerMap::value_type(clock()+trig->clocks,trig));
}

void stile::Timer::TimerImpl::AddTrigger (
	  TriggerCallback callback,
	  unsigned int identifier,
	  unsigned int milliseconds,
	  bool repeat,
	  bool allowDrift
	)
{
	//TODO Reject triggers with the same id

	assert (identifier != 0);
	TriggerObject* trig = new TriggerObject;
	trig->identifier = identifier;
	trig->strIdentifier[0] = '\0';
	trig->milliseconds = milliseconds;
	trig->clocks = (unsigned int)floor((CLOCKS_PER_SEC/1000)*milliseconds);
	trig->repeat = repeat;
	trig->allowDrift  = allowDrift;
	trig->triggerCallback = callback;
	trig->handler = NULL;
	_triggerMap.insert (TriggerMap::value_type(clock()+trig->clocks,trig));
}

void stile::Timer::TimerImpl::AddTrigger (
	  stile::TimerHandler* handler,
	  const char* identifier,
	  unsigned int milliseconds,
	  bool repeat,
	  bool allowDrift
	)
{
	assert(identifier[0] != '\0');
	TriggerObject* trig = new TriggerObject;
	strcpy(trig->strIdentifier,identifier);
	trig->milliseconds = milliseconds;
	trig->clocks = (unsigned int)floor((CLOCKS_PER_SEC/1000)*milliseconds);
	trig->repeat = repeat;
	trig->allowDrift = allowDrift;
	trig->triggerCallback = NULL;
	trig->handler = handler;
	_triggerMap.insert (TriggerMap::value_type(clock()+trig->clocks,trig));
}

void stile::Timer::TimerImpl::AddTrigger  (
	  stile::TimerHandler* handler,
	  unsigned int identifier,
	  unsigned int milliseconds,
	  bool repeat,
	  bool allowDrift
	)
{
	//TODO Reject triggers with the same id

	assert (identifier != 0);
	TriggerObject* trig = new TriggerObject;
	trig->identifier = identifier;
	trig->strIdentifier[0] = '\0';
	trig->milliseconds = milliseconds;
	trig->clocks = (unsigned int)floor((CLOCKS_PER_SEC/1000)*milliseconds);
	trig->repeat = repeat;
	trig->allowDrift  = allowDrift;
	trig->triggerCallback = NULL;
	trig->handler = handler;
	_triggerMap.insert (TriggerMap::value_type(clock()+trig->clocks,trig));
}

void stile::Timer::TimerImpl::RemoveTrigger  (
	  const char* identifier
	)
{
	TriggerMap::iterator i = _triggerMap.begin();
	while (i != _triggerMap.end())
	{
	  if (strcmp(i->second->strIdentifier, identifier)==0)
	  {
	    i->second->milliseconds = 0;
	  }
	  i++;
	}
}

void stile::Timer::TimerImpl::RemoveTrigger  (
	  unsigned int identifier
	)
{
	TriggerMap::iterator i = _triggerMap.begin();
	while (i != _triggerMap.end())
	{
	  if (i->second->identifier == identifier)
	  {
	    i->second->milliseconds = 0;
	  }
	  i++;
	}
}

void stile::Timer::TimerImpl::RemoveTrigger  (
	  stile::TimerHandler* handler
	)
{
	TriggerMap::iterator i = _triggerMap.begin();
	while (i != _triggerMap.end())
	{
	  if (i->second->handler == handler)
	  {
	    i->second->milliseconds = 0;
	  }
	  i++;
	}
}

stile::Timer::Timer  ()
	: _impl(*new stile::Timer::TimerImpl(this))
{
}

float stile::Timer::GetSeconds()
{
	return _impl._seconds;
}

void stile::Timer::AddTrigger  (
	  TriggerCallback callback,
	  const char* identifier,
	  unsigned int milliseconds,
	  bool repeat,
	  bool allowDrift
	)
{
	_impl.AddTrigger(callback, identifier, milliseconds, repeat, allowDrift);
}

void stile::Timer::AddTrigger  (
	  TriggerCallback callback,
	  unsigned int identifier,
	  unsigned int milliseconds,
	  bool repeat,
	  bool allowDrift
	)
{
	_impl.AddTrigger(callback, identifier, milliseconds, repeat, allowDrift);
}

void stile::Timer::AddTrigger  (
	  stile::TimerHandler* handler,
	  const char* identifier,
	  unsigned int milliseconds,
	  bool repeat,
	  bool allowDrift
	)
{
	_impl.AddTrigger(handler, identifier, milliseconds, repeat, allowDrift);
}

void stile::Timer::AddTrigger  (
	  stile::TimerHandler* handler,
	  unsigned int identifier,
	  unsigned int milliseconds,
	  bool repeat,
	  bool allowDrift
	)
{
	_impl.AddTrigger(handler, identifier, milliseconds, repeat, allowDrift);
}

void stile::Timer::RemoveTrigger  (
	  unsigned int identifier
	)
{
	_impl.RemoveTrigger(identifier);
}

void stile::Timer::RemoveTrigger  (
	  const char* identifier
	)
{
	_impl.RemoveTrigger(identifier);
}

void stile::Timer::RemoveTrigger  (
	  stile::TimerHandler* handler
	)
{
	_impl.RemoveTrigger(handler);
}

void stile::Timer::Update()
{
	_impl.Update();
}
