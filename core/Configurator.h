#ifndef STILE_CONFIGURATOR
#define STILE_CONFIGURATOR

#include <map>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include <cstring>
#include "Logger.h"
#include "Timer.h"
#include "ConfigListener.h"

namespace stile
{
	class Configurator;
}

using namespace std;

class stile::Configurator
{
public:
				Configurator		(
										Timer* timer,
										Logger* logger,
										const char* filename
									);

				~Configurator		();

	bool		GetBooleanOption	(const char* option);
	void		SetBooleanOption	(const char* option, bool value);
	string&		GetStringOption		(const char* option);
	void		SetStringOption		(const char* option, string& value);
	int			GetIntegerOption	(const char* option);
	void		SetIntegerOption	(const char* option, int value);
	float		GetFloatOption		(const char* option);
	void		SetFloatOption		(const char* option, float value);
	void		LoadConfiguration	();
	void		SaveConfiguration	();
	bool		Fail				();

private:
	struct ConfiguratorImpl;
	ConfiguratorImpl& _impl;

	Configurator (Configurator& copy);
	Configurator operator= (Configurator& rhs);
};


#endif //STILE_CONFIGURATOR
