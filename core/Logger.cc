/*
&&LICENSE
COPYRIGHT (C)2010 - RYAN GRIEVE
LICENSE&&
*/


#include "Logger.h"

struct stile::Logger::LoggerImpl
{
public:
	Logger&                     _parent;
	Timer&                      _timer;
	char                        _internalBuffer[4096];
	char                        _formattedTime[13];
	float*                      _elapsedTime;
	unsigned int                _debugLevel;
	bool                        _stdOut;
	std::vector<OutputHandler*> _outputHandlers;
	std::vector<OutputCallback> _outputCallbacks;

	LoggerImpl(
	    Logger& parent,
	    Timer* timer
	  )
	  : _parent (parent)
	  , _timer  (*timer)
	{
	  _debugLevel  = 0;
	  _stdOut  = false;
	};
	~LoggerImpl  (){};

	void Write  (const char* prefix, const char* output);
};

void stile::Logger::LoggerImpl::Write  (
	  const char* prefix,
	  const char* output
	)
{
	char formattedTime[16];
	sprintf (formattedTime, "%.3f", _timer.GetSeconds());
	std::ostringstream strOutput;
	strOutput <<"(" << formattedTime << ") " << prefix << ": " <<output << std::endl;
	if (_stdOut)
	{
	  std::cout << strOutput.str();
	}

	  for (unsigned int x=0; x< _outputHandlers.size(); x++)
	  {
	    _outputHandlers[x]->Write(strOutput);
	  }

	  for (unsigned int x=0; x< _outputCallbacks.size(); x++)
	  {
	    _outputCallbacks[x](strOutput);
	  }

}

stile::Logger::Logger  (
	  stile::Timer* timer
	)
	: _impl (*new LoggerImpl(*this, timer))
{
}

stile::Logger::~Logger ()
{
}

void stile::Logger::SetDebugLevel  (
	  unsigned int level
	)
{
	_impl._debugLevel = level;
	Debug(level, "Debug level changed.");
}

void stile::Logger::EnableStdOut  (
	  bool enable
	)
{
	_impl._stdOut = enable;
	if (enable)
	{
	  Debug(1,"Standard out has been enabled.");
	}
	else
	{
	  Debug(1,"Standard out has been disabled.");
	}
}

void stile::Logger::AddOutputCallback  (
	  stile::OutputCallback cb
	)
{
	for (unsigned int x=0;x<_impl._outputCallbacks.size();x++)
	{
	  if (_impl._outputCallbacks[x] == cb)
	  {
	    Warning("Logger: Output callback already added: 0x%x", cb);
	    return;
	  }
	}
	_impl._outputCallbacks.push_back(cb);
	Debug(3, "Logger: Output callback added: 0x%x", cb);
}
void stile::Logger::RemoveOutputCallback  (
	  stile::OutputCallback cb
	)
{
	for (unsigned int x=0;x<_impl._outputCallbacks.size();x++)
	{
	  if (_impl._outputCallbacks[x] == cb)
	  {
	    _impl._outputCallbacks.erase(_impl._outputCallbacks.begin()+x);
	    Debug(3, "Logger: Output callback removed: 0x%x", cb);
	    return;
	  }
	}
	Warning("Logger: Cannot remove output callback: 0x%x", cb);
}

void stile::Logger::AddOutputListener  (
	  stile::OutputHandler* listener
	)
{
	for (unsigned int x=0;x<_impl._outputHandlers.size();x++)
	{
	  if (_impl._outputHandlers[x] == listener)
	  {
	    Warning("Logger: Output listener already added: 0x%x", listener);
	    return;
	  }
	}
	_impl._outputHandlers.push_back(listener);
	Debug(3, "Logger: Output listener added: 0x%x", listener);
}

void stile::Logger::RemoveOutputListener  (
	  stile::OutputHandler* listener
	)
{
	for (unsigned int x=0;x<_impl._outputHandlers.size();x++)
	{
	  if (_impl._outputHandlers[x] == listener)
	  {
	    _impl._outputHandlers.erase(_impl._outputHandlers.begin()+x);
	    Debug(3, "Logger: Output listener removed: 0x%x", listener);
	    return;
	  }
	}
	Warning("Logger: Cannot remove output listener: 0x%x", listener);
}

void stile::Logger::Error (
	  const char* format,
	  ...
	)
{
	va_list vl;
	va_start (vl, format);
	vsprintf(_impl._internalBuffer, format, vl);
	va_end(vl);

	_impl.Write("ERROR", _impl._internalBuffer);
}

void stile::Logger::Warning (
	  const char* format,
	  ...
	)
{
	va_list vl;
	va_start (vl, format);
	vsprintf(_impl._internalBuffer, format, vl);
	va_end(vl);

	_impl.Write("WARNING", _impl._internalBuffer);
}

void stile::Logger::Info  (
	  const char* format,
	  ...
	)
{
	va_list vl;
	va_start (vl, format);
	vsprintf(_impl._internalBuffer, format, vl);
	va_end(vl);

	_impl.Write("INFO", _impl._internalBuffer);
}

void stile::Logger::Debug  (
	  unsigned int level,
	  const char* format,
	  ...
	)
{
	if (level > _impl._debugLevel)
	  return;

	va_list vl;
	va_start (vl, format);
	vsprintf(_impl._internalBuffer, format, vl);
	va_end(vl);
	char prefix[9];
	sprintf(prefix,"DEBUG[%d]", level);
	_impl.Write(prefix, _impl._internalBuffer);
}
