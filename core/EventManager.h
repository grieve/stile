/*
&&LICENSE
COPYRIGHT (C)2010 - RYAN GRIEVE
COPYRIGHT (C)2010 - RYAN GRIEVE
LICENSE&&
*/


#ifndef STILE_EVENT_MANAGER
#define STILE_EVENT_MANAGER

#include <vector>
#include "InputHandler.h"
#include "Timer.h"
#include "Logger.h"
#include "Configurator.h"
#include "SFML/Window.hpp"

namespace stile
{
	enum GameState
	{
		GAME_STATE_NORMAL,
		GAME_STATE_PAUSED,
		GAME_STATE_SUSPENDED,
		GAME_STATE_OVER,
		GAME_STATE_EXIT,
		WINDOW_STATE_FOCUS,
		WINDOW_STATE_BLUR
	};
	enum MouseEventType
	{
		MOUSE_LEFT,
		MOUSE_RIGHT,
		MOUSE_MIDDLE,
		MOUSE_X1,
		MOUSE_X2,
		MOUSE_FOCUS
	};
	class EventManager;
}

class stile::EventManager
{
	typedef void (*StateChangeCallback) (GameState);

public:
				EventManager				(
												Timer* timer,
												Logger* logger,
												Configurator* config
											);
				~EventManager				();
	void		AddKeyboardListener			(InputHandler* listener);
	void		RemoveKeyboardListener		(InputHandler* listener);
	void		AddMouseListener			(InputHandler* listener);
	void		RemoveMouseListener			(InputHandler* listener);
	void		AddGameStateCallback		(StateChangeCallback callback);
	void		RemoveGameStateCallback		(StateChangeCallback callback);
	void		SetGameState				(GameState gameState);
	GameState	GetGameState				();
	bool		KeyPressed					(int key);
	void		HandleKeyEvent				(
												unsigned int key,
												int x,
												int y,
												bool state,
												bool alt,
												bool ctrl,
												bool shift
											);
	void		HandleMouseEvent			(
												MouseEventType event,
												int state,
												int x,
												int y,
												bool alt,
												bool ctrl,
												bool shift
											);
	void		HandleMouseMovement			(int x, int y);
	void		Shutdown					(int exitCode);
	void		ProcessWindowEvents			(sf::Window* window);

private:
	struct EventManagerImpl;
	EventManagerImpl& _impl;

	EventManager (EventManager& copy);
	EventManager operator= (EventManager& rhs);
};
#endif //STILE_EVENT_MANAGER
