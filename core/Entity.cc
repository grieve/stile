
#include "Entity.h"

stile::Entity::Entity
	(
	  float x,
	  float y,
	  float w,
	  float h,
	  sf::Image* image
	)
	:_control(stile::GetControl())
{
	strcpy(_name, "Unknown Entity");
	_enabled = true;
	if (image != NULL)
	{
	  SetImage(image);
	}
	SetPosition(x, y);
	SetSize(w, h);
};

stile::Entity::~Entity  ()
{
}

void stile::Entity::Update (unsigned int elapsed)
{
	if (_acceleration.x == 0 && _acceleration.y == 0)
	{
	  _velocity += _drag;
	}
	else
	{
	  _velocity += _acceleration;
	}

	MoveBy(_velocity);
	Rotate(_angularVelocity);
	UpdateBoundingBox();
}

void stile::Entity::Render (sf::RenderTarget* target)
{
	target->Draw(*this);
}

void stile::Entity::DebugDraw (sf::RenderTarget* target)
{
	target->Draw(_bounding);
}

void stile::Entity::UpdateBoundingBox ()
{
	_bounding.SetPosition(GetPosition());
	_bounding.SetRotation(GetRotation());
}

void stile::Entity::SetImage (sf::Image* image)
{
}

sf::Image* stile::Entity::GetImage ()
{
}

void stile::Entity::SetAngularVelocity (float velocity)
{
	_angularVelocity = velocity;
}

float stile::Entity::GetAngularVelocity ()
{
	return _angularVelocity;
}

void stile::Entity::SetSize(float x, float y)
{
	Resize(x, y);
	sf::Vector2f pos = GetPosition();
	_bounding = sf::Shape::Rectangle
	  (
	    0,
	    0,
	    x,
	    y,
	    sf::Color(0, 0, 0, 0),
	    1,
	    sf::Color(0, 255, 0)
	  );
}

void stile::Entity::SetSize (stile::math::Vector2D size)
{
	SetSize(size.x, size.y);
}

void stile::Entity::SetVelocity (float x, float y)
{
	_velocity.x = x;
	_velocity.y = y;
}

void stile::Entity::SetVelocity (stile::math::Vector2D velocity)
{
	_velocity.x = velocity.x;
	_velocity.y = velocity.y;
}

void stile::Entity::SetAcceleration (float x, float y)
{
	_acceleration.x = x;
	_acceleration.y = y;
}

void stile::Entity::SetAcceleration (stile::math::Vector2D acceleration)
{
	_acceleration.x = acceleration.x;
	_acceleration.y = acceleration.y;
}

void stile::Entity::SetDrag (float x, float y)
{
	_drag.x = x;
	_drag.y = y;
}

void stile::Entity::SetDrag (stile::math::Vector2D drag)
{
	_drag.x = drag.x;
	_drag.y = drag.y;
}

void stile::Entity::SetOrigin (float x, float y)
{
	sf::Sprite::SetOrigin(x, y);
	_bounding.SetOrigin(x, y);
}

void stile::Entity::SetOrigin (stile::math::Vector2D center)
{
	sf::Sprite::SetOrigin(center.x, center.y);
	_bounding.SetOrigin(center.x, center.y);
}

stile::math::Vector2D stile::Entity::GetSize ()
{
	sf::Vector2f size = sf::Sprite::GetSize();
	return stile::math::Vector2D(size.x, size.y);
}

stile::math::Vector2D stile::Entity::GetVelocity ()
{
	return _velocity;
}

stile::math::Vector2D stile::Entity::GetAcceleration ()
{
	return _acceleration;
}

stile::math::Vector2D stile::Entity::GetDrag ()
{
	return _drag;
}

stile::math::Vector2D stile::Entity::GetOrigin ()
{
	sf::Vector2f center = sf::Sprite::GetOrigin();
	return stile::math::Vector2D(center.x, center.y);
}

stile::math::Vector2D stile::Entity::Accelerate (float x, float y)
{
	_acceleration.x += x;
	_acceleration.y += y;
}

stile::math::Vector2D stile::Entity::Accelerate (stile::math::Vector2D delta)
{
	_acceleration += delta;
}

stile::math::Vector2D stile::Entity::MoveBy (float x, float y)
{
	Move(x, y);
	sf::Vector2f pos = GetPosition();
	return stile::math::Vector2D(pos.x, pos.y);
}

stile::math::Vector2D stile::Entity::MoveBy (stile::math::Vector2D delta)
{
	Move(delta.x, delta.y);
	sf::Vector2f pos = GetPosition();
	return stile::math::Vector2D(pos.x, pos.y);
}

stile::math::Vector2D stile::Entity::MoveTo (float x, float y)
{
	SetPosition(x, y);
	sf::Vector2f pos = GetPosition();
	return stile::math::Vector2D(pos.x, pos.y);
}

stile::math::Vector2D stile::Entity::MoveTo (stile::math::Vector2D position)
{
	SetPosition(position.x, position.y);
	sf::Vector2f pos = GetPosition();
	return stile::math::Vector2D(pos.x, pos.y);
}

void stile::Entity::Rotate(float rot)
{
	sf::Sprite::Rotate(rot);
}
