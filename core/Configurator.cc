#include "Configurator.h"

struct stile::Configurator::ConfiguratorImpl
{
public:
	Configurator&         _parent;
	Timer&                _timer;
	Logger&               _logger;
	char                  _configFile[256];
	bool                  _lastFailed;
	map<string, bool>     _booleanOptions;
	map<string, string>   _stringOptions;
	map<string, int>      _integerOptions;
	map<string, float>    _floatOptions;

	void    LoadConfiguration       ();
	void    HandleOptionInternally  (const char* option, bool value);
	void    HandleOptionInternally  (const char* option, string& value);
	void    HandleOptionInternally  (const char* option, int value);
	void    HandleOptionInternally  (const char* option, float& value);
	bool    GetBooleanOption        (const char* option);
	void    SetBooleanOption        (const char* option, bool value);
	string& GetStringOption         (const char* option);
	void    SetStringOption         (const char* option, string& value);
	int     GetIntegerOption        (const char* option);
	void    SetIntegerOption        (const char* option, int value);
	float   GetFloatOption          (const char* option);
	void    SetFloatOption          (const char* option, float value);

	ConfiguratorImpl  (
	                    Configurator& parent,
	                    const char* configFile,
	                    Timer* timer,
	                    Logger* logger
	                  )
	                  : _parent  (parent)
	                  , _timer  (*timer)
	                  , _logger  (*logger)
	{
	  strcpy (_configFile, configFile);
	  LoadConfiguration ();
	  _lastFailed = false;
	};

	~ConfiguratorImpl  ()
	{
	}
};

void stile::Configurator::ConfiguratorImpl::LoadConfiguration ()
{
	ifstream config (_configFile, ifstream::in);
	if (config.fail())
	{
	  _logger.Warning("Unable to load configuration file \"%s\".", _configFile);
	  return;
	}
	string  optionString;
	string  keyString;
	string  valueString;
	int     valueInt;
	float   valueFloat;
	int     equalsPos;

	while (getline(config, optionString))
	{
	  if (optionString.length() == 0)
	  {
	    continue;
	  }
	  optionString  = optionString.substr(optionString.find_first_not_of(" "));
	  if (optionString.c_str()[0]=='#')
	  {
	    //commented line - print at debug level 5 otherwise skip
	    if (optionString.length() > 1)
	    {
	      _logger.Debug(5, "Configuration comment: %s", optionString.substr(1).c_str());
	    }
	    continue;
	  }
	  equalsPos  = optionString.find_first_of("=");
	  keyString  = optionString.substr(0, equalsPos);
	  valueString  = optionString.substr(equalsPos+1);
	  if (valueString == "true")
	  {
	    HandleOptionInternally (keyString.c_str(), true);
	    SetBooleanOption(keyString.c_str(), true);
	  }
	  else
	  {
	    if (valueString == "false")
	    {
	      HandleOptionInternally (keyString.c_str(), false);
	      SetBooleanOption(keyString.c_str(), false);
	    }
	    else
	    {
	      if (valueString.find_first_not_of("0123456789.") < 1024)
	      {
	        HandleOptionInternally (keyString.c_str(), valueString);
	        SetStringOption(keyString.c_str(), valueString);
	      }
	      else
	      {
	        if (valueString.find_first_of('.') < 1024)
	        {
	          valueFloat = atof(valueString.c_str());
	          HandleOptionInternally (keyString.c_str(), valueFloat);
	          SetFloatOption(keyString.c_str(), valueFloat);
	        }
	        else
	        {
	          valueInt = atoi(valueString.c_str());
	          HandleOptionInternally (keyString.c_str(), valueInt);
	          SetIntegerOption(keyString.c_str(), valueInt);
	        }
	      }
	    }
	  }
	}
}

void stile::Configurator::ConfiguratorImpl::HandleOptionInternally  (
	  const char* option,
	  bool value
	)
{
	if (strcmp(option,"enableStdOut")==0)
	{
	  _logger.EnableStdOut(value);
	}
}

void stile::Configurator::ConfiguratorImpl::HandleOptionInternally  (
	  const char* option,
	  string& value
	)
{
}

void stile::Configurator::ConfiguratorImpl::HandleOptionInternally  (
	  const char* option,
	  int value
	)
{
	if (strcmp(option,"debugLevel")==0)
	{
	  _logger.SetDebugLevel(value);
	}
}

void stile::Configurator::ConfiguratorImpl::HandleOptionInternally  (
	  const char* option,
	  float& value
	)
{
}

stile::Configurator::Configurator  (
	  Timer* timer,
	  Logger* logger,
	  const char* configFile
	)
	: _impl(*new ConfiguratorImpl(*this, configFile, timer, logger))
{
}

stile::Configurator::~Configurator()
{
}

void stile::Configurator::LoadConfiguration ()
{
	_impl.LoadConfiguration();
}

void stile::Configurator::SaveConfiguration ()
{
}

bool stile::Configurator::ConfiguratorImpl::GetBooleanOption  (
	  const char* option
	)
{
	map<string,bool>::iterator iter = _booleanOptions.find(option);
	if (iter == _booleanOptions.end())
	{
	  _logger.Warning("Boolean option \"%s\" not found.", option);
	  _lastFailed = true;
	}
	else
	{
	  return iter->second;
	  _lastFailed = false;
	}
}

void stile::Configurator::ConfiguratorImpl::SetBooleanOption  (
	  const char* option,
	  bool value
	)
{
	map<string,bool>::iterator iter = _booleanOptions.find(option);
	if (iter == _booleanOptions.end())
	{
	  _booleanOptions.insert(map<string,bool>::value_type(option, value));
	}
	else
	{
	  (iter->second) = value;
	}
}

string& stile::Configurator::ConfiguratorImpl::GetStringOption  (
	  const char* option
	)
{
	map<string,string>::iterator iter = _stringOptions.find(option);
	if (iter == _stringOptions.end())
	{
	  _logger.Warning("String option \"%s\" not found.", option);
	  _lastFailed = true;
	}
	else
	{
	  return iter->second;
	  _lastFailed = false;
	}
}

void stile::Configurator::ConfiguratorImpl::SetStringOption  (
	  const char* option,
	  string& value
	)
{
	map<string,string>::iterator iter = _stringOptions.find(option);
	if (iter == _stringOptions.end())
	{
	  _stringOptions.insert(map<string,string>::value_type(option, value));
	}
	else
	{
	  (iter->second) = value;
	}
}

int stile::Configurator::ConfiguratorImpl::GetIntegerOption  (
	  const char* option
	)
{
	map<string, int>::iterator iter = _integerOptions.find(option);
	if (iter == _integerOptions.end())
	{
	  _logger.Warning("Integer option \"%s\" not found.", option);
	  _lastFailed = true;
	}
	else
	{
	  return iter->second;
	  _lastFailed = false;
	}
}

void stile::Configurator::ConfiguratorImpl::SetIntegerOption  (
	  const char* option,
	  int value
	)
{
	map<string, int>::iterator iter = _integerOptions.find(option);
	if (iter == _integerOptions.end())
	{
	  _integerOptions.insert(map<string, int>::value_type(option, value));
	}
	else
	{
	  (iter->second) = value;
	}
}

float stile::Configurator::ConfiguratorImpl::GetFloatOption  (
	  const char* option
	)
{
	map<string, float>::iterator iter = _floatOptions.find(option);
	if (iter == _floatOptions.end())
	{
	  _logger.Warning("Float option \"%s\" not found.", option);
	  _lastFailed = true;
	}
	else
	{
	  return iter->second;
	  _lastFailed = false;
	}
}

void stile::Configurator::ConfiguratorImpl::SetFloatOption  (
	  const char* option,
	  float value
	)
{
	map<string, float>::iterator iter = _floatOptions.find(option);
	if (iter == _floatOptions.end())
	{
	  _floatOptions.insert(map<string, float>::value_type(option, value));
	}
	else
	{
	  (iter->second) = value;
	}
}

bool stile::Configurator::GetBooleanOption  (
	  const char* option
	)
{
	return _impl.GetBooleanOption(option);
}

void stile::Configurator::SetBooleanOption  (
	  const char* option,
	  bool value
	)
{
	_impl.SetBooleanOption(option, value);
}

string& stile::Configurator::GetStringOption  (
	  const char* option
	)
{
	return _impl.GetStringOption(option);
}

void stile::Configurator::SetStringOption  (
	  const char* option,
	  string& value
	)
{
	_impl.SetStringOption(option, value);
}

int stile::Configurator::GetIntegerOption  (
	  const char* option
	)
{
	return _impl.GetIntegerOption(option);
}

void stile::Configurator::SetIntegerOption  (
	  const char* option,
	  int value
	)
{
	_impl.SetIntegerOption(option, value);
}

float stile::Configurator::GetFloatOption  (
	  const char* option
	)
{
	return _impl.GetFloatOption(option);
}

void stile::Configurator::SetFloatOption  (
	  const char* option,
	  float value
	)
{
	_impl.SetFloatOption (option, value);
}

bool stile::Configurator::Fail ()
{
	return _impl._lastFailed;
}
