/*
&&LICENSE
COPYRIGHT (C)2010 - RYAN GRIEVE
LICENSE&&
*/


#ifndef STILE_TIMER
#define STILE_TIMER

#include <time.h>
#include <math.h>
#include <string>
#include <cstring>
#include <map>
#include <vector>
#include <assert.h>
#include "TimerHandler.h"

namespace stile
{
	class Timer;
}

class stile::Timer
{
	typedef void (*TriggerCallback) (
	    unsigned int,
	    const char*,
	    unsigned int
	  );

public:
	      Timer  ();
	      ~Timer  ();
	float GetSeconds  ();
	void  AddTrigger    (
	                      TriggerCallback  callback,
	                      const char*  identifier,
	                      unsigned int  milliseconds,
	                      bool  repeat = false,
	                      bool  allowDrift = false
	                    );
	void  AddTrigger    (
	                      TriggerCallback  callback,
	                      unsigned int  identifier,
	                      unsigned int  milliseconds,
	                      bool  repeat = false,
	                      bool  allowDrift = false
	                    );
	void  AddTrigger    (
	                      stile::TimerHandler*  handler,
	                      const char*  identifier,
	                      unsigned int  milliseconds,
	                      bool  repeat = false,
	                      bool  allowDrift = false
	                    );
	void  AddTrigger    (
	                      stile::TimerHandler*  handler,
	                      unsigned int  identifier,
	                      unsigned int  milliseconds,
	                      bool  repeat = false,
	                      bool  allowDrift = false
	                    );
	void  RemoveTrigger (const char* identifier);
	void  RemoveTrigger (unsigned int identifier);
	void  RemoveTrigger (stile::TimerHandler* handler);
	void  Update        ();

private:
	struct  TimerImpl;
	TimerImpl&  _impl;

	Timer  (const Timer& copy);
	Timer operator=  (Timer& rhs);

};

#endif // STILE_TIMER
