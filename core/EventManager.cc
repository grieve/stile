/*
&&LICENSE
COPYRIGHT (C)2010 - RYAN GRIEVE
LICENSE&&
*/

#include "EventManager.h"

struct stile::EventManager::EventManagerImpl
{
public:
	EventManager&                     _parent;
	Timer&                            _timer;
	Logger&                           _logger;
	Configurator&                     _config;
	GameState                         _gameState;
	std::vector<StateChangeCallback>  _stateChangeCallbacks;
	std::vector<InputHandler*>        _keyboardListeners;
	std::vector<InputHandler*>        _mouseListeners;
	MouseEvent*                       _buttonStates  [10];
	sf::Window*                       _window;

	EventManagerImpl  (
	    EventManager* parent,
	    Timer* timer,
	    Logger* logger,
	    Configurator* config
	  )
	  : _parent  (*parent)
	  , _timer  (*timer)
	  , _logger  (*logger)
	  , _config  (*config)
	{
	  for (unsigned int x = 0; x < 10; x++)
	  {
	    _buttonStates[x]  = new MouseEvent;
	    _buttonStates[x]->button  = x;
	    _buttonStates[x]->state  = false;
	    _buttonStates[x]->x  = 0;
	    _buttonStates[x]->y  = 0;
	    _buttonStates[x]->secs  = 0;
	  }
	  _gameState = GAME_STATE_NORMAL;
	  _logger.Debug(1, "EventManager started correctly.");
	}
};

stile::EventManager::EventManager  (
	  Timer*        timer,
	  Logger*       logger,
	  Configurator* config
	)
	: _impl (*new EventManagerImpl(this, timer, logger, config))
{
}

stile::EventManager::~EventManager()
{
}

void stile::EventManager::AddKeyboardListener  (
	  InputHandler* listener
	)
{
	for (unsigned int x=0; x<_impl._keyboardListeners.size(); x++)
	{
	  if (_impl._keyboardListeners[x] == listener)
	  {
	    _impl._logger.Warning("EventManager: Keyboard listener already added: 0x%x", listener);
	    return;
	  }
	}
	_impl._keyboardListeners.push_back (listener);
	_impl._logger.Debug(3,"EventManager: New keyboard listener added: 0x%x", listener);
}

void stile::EventManager::RemoveKeyboardListener  (
	  InputHandler* listener
	)
{
	for (unsigned int x=0; x<_impl._keyboardListeners.size(); x++)
	{
	  if (_impl._keyboardListeners[x] == listener)
	  {
	    _impl._keyboardListeners.erase(_impl._keyboardListeners.begin()+x);
	    _impl._logger.Debug(3,"EventManager: Keyboard listener removed: 0x%x", listener);
	    return;
	  }
	}
	_impl._logger.Warning("EventManager: Cannot remove keyboard listener: 0x%x", listener);
}

void stile::EventManager::AddMouseListener  (
	  InputHandler* listener
	)
{
	for (unsigned int x=0; x<_impl._mouseListeners.size(); x++)
	{
	  if (_impl._mouseListeners[x] == listener)
	  {
	    _impl._logger.Warning("EventManager: Mouse listener already added: 0x%x", listener);
	    return;
	  }
	}
	_impl._mouseListeners.push_back (listener);
	_impl._logger.Debug(3, "EventManager: Mouse listener added: 0x%x", listener);
}

void stile::EventManager::RemoveMouseListener  (
	  InputHandler* listener
	)
{
	for (unsigned int x=0; x<_impl._mouseListeners.size(); x++)
	{
	  if (_impl._mouseListeners[x] == listener)
	  {
	    _impl._mouseListeners.erase(_impl._mouseListeners.begin()+x);
	    _impl._logger.Debug(3,"EventManager: Mouse listener removed: 0x%x", listener);
	    return;
	  }
	}
	_impl._logger.Warning("EventManager: Cannot remove mouse listener: 0x%x", listener);
}

void stile::EventManager::AddGameStateCallback  (
	  StateChangeCallback callback
	)
{
	for(unsigned int x=0; x<_impl._stateChangeCallbacks.size(); x++)
	{
	  if (_impl._stateChangeCallbacks[x] == callback)
	  {
	    _impl._logger.Warning("EventManager: State change callback already added: 0x%x", callback);
	    return;
	  }
	}
	_impl._stateChangeCallbacks.push_back(callback);
	_impl._logger.Debug(3,"EventManager: State change callback added: 0x%x", callback);
};

void stile::EventManager::RemoveGameStateCallback  (
	  StateChangeCallback callback
	)
{
	for(unsigned int x=0; x<_impl._stateChangeCallbacks.size(); x++)
	{
	  if (_impl._stateChangeCallbacks[x] == callback)
	  {
	    _impl._stateChangeCallbacks.erase(_impl._stateChangeCallbacks.begin()+x);
	    _impl._logger.Debug(3,"EventManager: State change callback removed: 0x%x", callback);
	    return;
	  }
	}
	_impl._logger.Warning("EventManager: Cannot remove state change callback: 0x%x", callback);
};

void stile::EventManager::SetGameState  (
	  GameState gameState
	)
{
	if (gameState == _impl._gameState)
	  return;

	_impl._gameState = gameState;
	for (unsigned int x=0; x<_impl._stateChangeCallbacks.size(); x++)
	{
	  (*_impl._stateChangeCallbacks[x])(gameState);
	}
};

stile::GameState stile::EventManager::GetGameState ()
{
	return _impl._gameState;
};

bool stile::EventManager::KeyPressed  (
	  int key
	)
{
}

void stile::EventManager::HandleKeyEvent (
	                                              unsigned int key,
	                                              int x,
	                                              int y,
	                                              bool state,
	                                              bool alt,
	                                              bool ctrl,
	                                              bool shift
	                                            )
{
	unsigned int intKey = (unsigned int)key;
	for (unsigned int i=0; i<_impl._keyboardListeners.size(); i++)
	{
	  _impl._keyboardListeners[i]->HandleKeyboardEvent (intKey, x, y, state, alt, ctrl, shift);
	}
}

void stile::EventManager::HandleMouseEvent  (
	  MouseEventType event,
	  int state,
	  int x,
	  int y,
	  bool alt,
	  bool ctrl,
	  bool shift
	)
{
	_impl._buttonStates[event]->secs  = _impl._timer.GetSeconds();
	_impl._buttonStates[event]->state  = state;
	_impl._buttonStates[event]->x  = x;
	_impl._buttonStates[event]->y  = y;
	for (unsigned int i=0; i <_impl._mouseListeners.size(); i++)
	{
	  _impl._mouseListeners[i]->HandleMouseEvent(event, state, x, y, alt, ctrl, shift);
	}
	_impl._logger.Debug(5,"EventManager: Mouse event (%d, %d, %d, %d)", event, state, x, y);
}

void stile::EventManager::HandleMouseMovement  (
	  int x,
	  int y
	)
{
	for (unsigned int i=0; i <_impl._mouseListeners.size(); i++)
	{
	  _impl._mouseListeners[i]->HandleMouseMovement(x, y, _impl._buttonStates);
	}
	std::cout << x << y << std::endl;
	_impl._logger.Debug(5,"EventManager: Mouse movement (%d, %d)", x, y);
}

void stile::EventManager::Shutdown  (
	  int exitCode
	)
{
	//TODO add shutdown callbacks
	_impl._logger.Debug(3, "NOTE: Shutdown callbacks not yet implemented.\nShutting down...");
	exit(exitCode);
}

void stile::EventManager::ProcessWindowEvents (sf::Window* window)
{
	sf::Event event;
	while(window->PollEvent(event))
	{
	  switch(event.Type)
	  {
	    case sf::Event::Closed :
	      SetGameState(stile::GAME_STATE_EXIT);
	      Shutdown(0);
	      break;
	    case sf::Event::MouseLeft :
	      HandleMouseEvent(
	                        stile::MOUSE_FOCUS,
	                        false,
	                        sf::Mouse::GetPosition(*window).x,
	                        sf::Mouse::GetPosition(*window).y,
	                        (sf::Keyboard::IsKeyPressed(sf::Keyboard::LAlt)     || sf::Keyboard::IsKeyPressed(sf::Keyboard::RAlt)),
	                        (sf::Keyboard::IsKeyPressed(sf::Keyboard::LShift)   || sf::Keyboard::IsKeyPressed(sf::Keyboard::RShift)),
	                        (sf::Keyboard::IsKeyPressed(sf::Keyboard::LControl) || sf::Keyboard::IsKeyPressed(sf::Keyboard::RControl))
	                      );
	      break;
	    case sf::Event::MouseEntered :
	      HandleMouseEvent(
	                        stile::MOUSE_FOCUS,
	                        true,
	                        sf::Mouse::GetPosition(*window).x,
	                        sf::Mouse::GetPosition(*window).y,
	                        (sf::Keyboard::IsKeyPressed(sf::Keyboard::LAlt)     || sf::Keyboard::IsKeyPressed(sf::Keyboard::RAlt)),
	                        (sf::Keyboard::IsKeyPressed(sf::Keyboard::LShift)   || sf::Keyboard::IsKeyPressed(sf::Keyboard::RShift)),
	                        (sf::Keyboard::IsKeyPressed(sf::Keyboard::LControl) || sf::Keyboard::IsKeyPressed(sf::Keyboard::RControl))
	                      );
	      break;
	    case sf::Event::MouseButtonPressed :
	      HandleMouseEvent(
	                        (stile::MouseEventType)event.MouseButton.Button,
	                        true,
	                        sf::Mouse::GetPosition(*window).x,
	                        sf::Mouse::GetPosition(*window).y,
	                        (sf::Keyboard::IsKeyPressed(sf::Keyboard::LAlt)     || sf::Keyboard::IsKeyPressed(sf::Keyboard::RAlt)),
	                        (sf::Keyboard::IsKeyPressed(sf::Keyboard::LShift)   || sf::Keyboard::IsKeyPressed(sf::Keyboard::RShift)),
	                        (sf::Keyboard::IsKeyPressed(sf::Keyboard::LControl) || sf::Keyboard::IsKeyPressed(sf::Keyboard::RControl))
	                      );
	      break;
	    case sf::Event::MouseButtonReleased :
	      HandleMouseEvent(
	                        (stile::MouseEventType)event.MouseButton.Button,
	                        false,
	                        sf::Mouse::GetPosition(*window).x,
	                        sf::Mouse::GetPosition(*window).y,
	                        (sf::Keyboard::IsKeyPressed(sf::Keyboard::LAlt)     || sf::Keyboard::IsKeyPressed(sf::Keyboard::RAlt)),
	                        (sf::Keyboard::IsKeyPressed(sf::Keyboard::LShift)   || sf::Keyboard::IsKeyPressed(sf::Keyboard::RShift)),
	                        (sf::Keyboard::IsKeyPressed(sf::Keyboard::LControl) || sf::Keyboard::IsKeyPressed(sf::Keyboard::RControl))
	                      );
	      break;
	    case sf::Event::KeyPressed :
	      HandleKeyEvent(
	                      event.Key.Code,
	                      sf::Mouse::GetPosition(*window).x,
	                      sf::Mouse::GetPosition(*window).y,
	                      true,
	                      (sf::Keyboard::IsKeyPressed(sf::Keyboard::LAlt)     || sf::Keyboard::IsKeyPressed(sf::Keyboard::RAlt)),
	                      (sf::Keyboard::IsKeyPressed(sf::Keyboard::LShift)   || sf::Keyboard::IsKeyPressed(sf::Keyboard::RShift)),
	                      (sf::Keyboard::IsKeyPressed(sf::Keyboard::LControl) || sf::Keyboard::IsKeyPressed(sf::Keyboard::RControl))
	                    );
	      break;
	    case sf::Event::KeyReleased :
	      HandleKeyEvent(
	                      event.Key.Code,
	                      sf::Mouse::GetPosition(*window).x,
	                      sf::Mouse::GetPosition(*window).y,
	                      false,
	                      (sf::Keyboard::IsKeyPressed(sf::Keyboard::LAlt)     || sf::Keyboard::IsKeyPressed(sf::Keyboard::RAlt)),
	                      (sf::Keyboard::IsKeyPressed(sf::Keyboard::LShift)   || sf::Keyboard::IsKeyPressed(sf::Keyboard::RShift)),
	                      (sf::Keyboard::IsKeyPressed(sf::Keyboard::LControl) || sf::Keyboard::IsKeyPressed(sf::Keyboard::RControl))
	                    );
	      break;
	  }
	}
}
