/*
&&LICENSE
COPYRIGHT (C)2010 - RYAN GRIEVE
LICENSE&&
*/


#ifndef STILE_LOGGER
#define STILE_LOGGER

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <stdarg.h>
#include <map>
#include <math.h>
#include <string>
#include <cstring>
#include <vector>
#include "Types.h"
#include "Timer.h"
#include "OutputHandler.h"

namespace stile
{
	class Logger;
}

class stile::Logger
{
public:
	      Logger                (Timer* timer);
	      ~Logger                ();
	void  SetDebugLevel          (unsigned int level);
	void  EnableStdOut          (bool enable);
	void  AddOutputListener      (OutputHandler* listener);
	void  RemoveOutputListener  (OutputHandler* listener);
	void  AddOutputCallback      (OutputCallback cb);
	void  RemoveOutputCallback  (OutputCallback cb);
	void  Error                  (const char* format, ...);
	void  Warning                (const char* format, ...);
	void  Info                  (const char* format, ...);
	void  Debug                  (unsigned int level, const char* format, ...);

private:

	struct  LoggerImpl;
	LoggerImpl&  _impl;

	Logger  (const Logger& copy);
	Logger operator=  (Logger& rhs);
};

#endif //STILE_LOGGER
