#ifndef STILE_WORLD
#define STILE_WORLD

#include "Entity.h"

namespace stile
{
	class   World;
	class   Entity;
	class   Engine;
	struct  Control;
}

class stile::World
{
private:
	stile::Control&         _control;
	bool                    _enabled;
	bool                    _debug;
	char                    _name[128];
	vector<stile::Entity*>  _children;

public:
	                        World    ();
	                        World    (char* id, bool enable=true);
	virtual                  ~World  ();
	virtual void            Init    ();
	virtual void            Render  (sf::RenderTarget* target);
	virtual void            Update  (unsigned int elapsed);
	virtual bool            Add      (stile::Entity* child);
	virtual bool            Remove  (stile::Entity* child);
	        void            Debug   (bool enable = true);
};



#endif //STILE_WORLD
