#ifndef STILE_ENGINE
#define STILE_ENGINE

#include <SFML/Graphics/RenderWindow.hpp>
#include "World.h"
#include "TimerHandler.h"

namespace stile
{
	class	Engine;
	class	World;
	class	Entity;
	struct	Control;
}

class stile::Engine : public TimerHandler
{
private:
	stile::Control&		_control;
	stile::World*		_world;
	sf::RenderWindow*	_window;
	unsigned int		_frameRate;
	unsigned int		_updateRate;
	bool				_paused;
	bool				_stopped;

public:
	void			Render			();
	void			Update			(unsigned int elapsed);
	void			SetWorld		(stile::World* world);
	stile::World*	GetWorld		();
	void			SetFrameRate	(unsigned int frameRate);
	void			SetUpdateRate	(unsigned int updateRate);
	void			Stop			();
	void			Pause			();
	void			Resume			();
	void			Trigger			(
										unsigned int	identifier,
										const char*		strIdentifier,
										unsigned int	millis
									);
					Engine			(
										const char*		title,
										unsigned int	width		= 640,
										unsigned int	height		= 480,
										unsigned int	updateRate	= 100,
										unsigned int	frameRate	= 60
									);
					~Engine			();
};

#endif //STILE_ENGINE
