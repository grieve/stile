
#include "World.h"

stile::World::World  ()
	:_control(stile::GetControl())
{
	strcpy(_name, "Unknown World");
	_enabled = true;
	_debug = false;
	Init();
};

stile::World::World  (
	  char* id,
	  bool enable
	)
	:_control(stile::GetControl())
{
	strcpy(_name, id);
	_enabled = enable;
	_debug = false;
	Init();
}

stile::World::~World  ()
{
}

void stile::World::Init  ()
{

}

void stile::World::Debug (bool enable)
{
	_debug = enable;
}

void stile::World::Render  (sf::RenderTarget* target)
{
	for(int x=0; x< _children.size(); x++)
	{
	  _children[x]->Render(target);
	  if(_debug)
	  {
	    _children[x]->DebugDraw(target);
	  }
	}
}

void stile::World::Update (unsigned int elapsed)
{
	for(int x=0; x< _children.size(); x++)
	{
	  _children[x]->Update(elapsed);
	}
}

bool stile::World::Add (stile::Entity* child)
{
	for(int x=0; x< _children.size(); x++)
	{
	  if(_children[x] == child)
	  {
	    _control.log->Warning("Child already exists in this world. (%X)", child);
	    return false;
	  }
	}
	_children.push_back(child);
	return true;
}

bool stile::World::Remove (stile::Entity* child)
{
	for(int x=0; x< _children.size(); x++)
	{
	  if(_children[x] == child)
	  {
	    _children.erase(_children.begin()+x);
	    return false;
	  }
	}
	_control.log->Warning("Child does not exist in this world. (%X)", child);
	return true;
}
